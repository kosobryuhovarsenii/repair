$(document).ready(function(){
// Валидация форм
  $("#modal-form").validate({
      rules: {
        username: {
          required: true,
          minlength: 2,
          maxlength: 16
        },
        phone: {
          required: true
        },
      },
      messages: {
        username: "Заполните поле",
        phone: "Заполните поле"
      },
      submitHandler: function(form) {
        // Показ модального окна
        form.reset();
        $("#dialog").removeClass("modal-dialog_active");
        $("#success").addClass("modal-success_active");
        setTimeout(function(){
          $("#success").removeClass("modal-success_active")
          $("#modal").removeClass("modal_active")
        }, 3000);
      }
  });

  $('#offer-form').validate({
    rules: {
      username: {
        required: true,
        minlength: 2,
        maxlength: 16
      },
      phone: {
        required: true
      }
    },
    messages: {
      username: "Заполните поле",
      phone: "Заполните поле"
    },
    submitHandler: function(form) {
      // Показ модального окна
      form.reset();
      $("#success").addClass("modal-success_active");
      $("#modal").addClass("modal_active");
      setTimeout(function(){
        $("#success").removeClass("modal-success_active")
        $("#modal").removeClass("modal_active")
      }, 3000);
    }
  });

  $('#brif-form').validate({
    rules: {
      username: {
        required: true,
        minlength: 2,
        maxlength: 16
      },
      phone: {
        required: true
      }
    },
    messages: {
      username: "Заполните поле",
      phone: "Заполните поле"
    },
    submitHandler: function(form) {
      // Показ модального окна
      form.reset();
      $("#success").addClass("modal-success_active");
      $("#modal").addClass("modal_active");
      setTimeout(function(){
        $("#success").removeClass("modal-success_active")
        $("#modal").removeClass("modal_active")
      }, 3000);
    }
  });

  // Обработка и отправка форм через AJAX
  $("#modal-form").on("submit", function(event){
    event.preventDefault();
     $.ajax({
      type: "POST",
      url: "mail.php",
      data: $(this).serialize(),
      success: function(data){
        $(".modal-success__title").html("Спасибо за заявку, скоро мы вам перезвоним!");
      },
      error: function(jqXHR, textStatus){
        console.log(jqXHR + ": " + textStatus);
      }
    });
  });

  $("#offer-form").on("submit", function(event){
    event.preventDefault();
     $.ajax({
      type: "POST",
      url: "mail.php",
      data: $(this).serialize(),
      success: function(data){
        console.log(data),
        $(".modal-success__title").html("Спасибо за заявку, скоро мы вам перезвоним!");
      },
      error: function(jqXHR, textStatus){
        console.log(jqXHR + ": " + textStatus);
      }
    });
  });

  $("#brif-form").on("submit", function(event){
    event.preventDefault();
     $.ajax({
      type: "POST",
      url: "mail.php",
      data: $(this).serialize(),
      success: function(data){
        $(".modal-success__title").html("Спасибо за заявку, скоро мы вам перезвоним!");
      },
      error: function(jqXHR, textStatus){
        console.log(jqXHR + ": " + textStatus);
      }
    });
  });
});