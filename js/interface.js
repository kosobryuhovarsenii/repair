$(document).ready(function(){
  var arrow = $("#arrow");

  arrow.on('click', function() {
    $('html, body').animate({scrollTop:0}, 1000);
  });

  $(window).scroll(function(){
    if ($(window).scrollTop() < 150){
      arrow.addClass("arrow_active");
      $(arrow).hide(500);
    } else {
      arrow.removeClass("arrow_active");
      $(arrow).show(500);
    };
  });

});